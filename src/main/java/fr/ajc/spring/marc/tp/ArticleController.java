package fr.ajc.spring.marc.tp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/articles")
public class ArticleController {

	@Autowired
	private ArticleRepository articleRepository;

	@GetMapping
	public Iterable<Article> findAll() {
		return articleRepository.findAll();
	}

	@GetMapping("/article/{name}")
	public List findByName(@PathVariable String name) {
		return articleRepository.findByName(name);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Article create(Article article) {
		return articleRepository.save(article);
	}

	@DeleteMapping
	public String delete(long id) {
		articleRepository.deleteById(id);
		return "HTTP DELETE was called";
	}

}