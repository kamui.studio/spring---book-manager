package fr.ajc.spring.marc.tp;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface ArticleRepository extends CrudRepository<Article, Long> {

	List<Article> findByName(String name);
	List<Article> findByNameContainingIgnoreCaseOrEditorContainingIgnoreCaseOrAuthorContainingIgnoreCaseOrSynopsisContainingIgnoreCase(String queryName, String queryEditor, String queryAuthor, String querySynopsis);
	
}
