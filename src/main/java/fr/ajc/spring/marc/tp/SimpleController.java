package fr.ajc.spring.marc.tp;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.thymeleaf.extras.java8time.dialect.Java8TimeDialect;
import org.thymeleaf.spring5.ISpringTemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ITemplateResolver;

import fr.ajc.spring.marc.tp.Season; 

@Controller
public class SimpleController {

	public static final String winter = "winter";
	public static final String spring = "spring";
	public static final String summer = "summer";
	public static final String fall   = "fall";
	
	@Value("${spring.application.name}")
	String appName;


	@GetMapping("/")
	public String homePage(Model model) {
		model.addAttribute("appName", appName);
		return "home";
	}

	@GetMapping("/serverDate")
	public String serverDate(Model model) {
		model.addAttribute("localDateTime", LocalDateTime.now());
		model.addAttribute("season", seasonFromDate(new Date()));
		return "serverDate";
	}

	@GetMapping("/seasons")
	public String seasons(Model model) {
		
		List<Season> seasons = new ArrayList<Season>();
		
		seasons.add(new Season(winter, currentSeason(new Date(), winter)));
		seasons.add(new Season(spring, currentSeason(new Date(), spring)));
		seasons.add(new Season(summer, currentSeason(new Date(), summer)));
		seasons.add(new Season(fall,   currentSeason(new Date(), fall)));
		
		model.addAttribute("seasons", seasons);
		return "seasons";

	}


	// Get boolean if season is current from current date
	private boolean currentSeason(Date date, String season) {
		
		SimpleDateFormat formating = new SimpleDateFormat("D");
		int yearDay = Integer.parseInt(formating.format(date));
		String name;
		
		if (yearDay >= 79 && yearDay <= 171) {
			name = spring;
		} else if (yearDay >= 172 && yearDay <= 265) {
			name = summer;
		} else if (yearDay >= 266 && yearDay <= 355) {
			name = fall;
		} else {
			name = winter;
		}
		
		if (season.equals(name)) {
			return true;
		}
		
		return false;

	}
	
	// Get season from current date
	private String seasonFromDate(Date date) {
		
		SimpleDateFormat formating = new SimpleDateFormat("D");
		int yearDay = Integer.parseInt(formating.format(date));
		String season;
		
		if (yearDay >= 79 && yearDay <= 171) {
			season = "spring";
		} else if (yearDay >= 172 && yearDay <= 265) {
			season = "summer";
		} else if (yearDay >= 266 && yearDay <= 355) {
			season = "fall";
		} else {
			season = "winter";
		}
		
		return season;
	}

	// Allow Thymleaf to manipulate dates
	private ISpringTemplateEngine templateEngine(ITemplateResolver templateResolver) {
		SpringTemplateEngine engine = new SpringTemplateEngine();
		engine.addDialect(new Java8TimeDialect());
		engine.setTemplateResolver(templateResolver);
		return engine;
	}
	
}
