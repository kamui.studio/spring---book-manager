package fr.ajc.spring.marc.tp;

public class Season {

	private String name;
	private boolean current;

	public Season(String name, boolean current) {
		this.name = name;
		this.current = current;
	}

	public Season() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isCurrent() {
		return current;
	}

	public void setCurrent(boolean current) {
		this.current = current;
	}

	public String toString() {
		return "Seasons [name=" + name + ", current=" + current + "]";
	}
	
}
