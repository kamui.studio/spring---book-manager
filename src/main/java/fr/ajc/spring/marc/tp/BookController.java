package fr.ajc.spring.marc.tp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/articles")
public class BookController {

	@Autowired
	private ArticleRepository articleRepository;


	@GetMapping
	public String findAll(Model model) {
		model.addAttribute("books", articleRepository.findAll());
		return "list";
	}


	@GetMapping("/search")
	public String search(@RequestParam String query, Model model) {
		model.addAttribute("books", articleRepository.findByNameContainingIgnoreCaseOrEditorContainingIgnoreCaseOrAuthorContainingIgnoreCaseOrSynopsisContainingIgnoreCase(query, query, query, query));
		return "list";
	}
	
	@GetMapping("/article/{name}")
	public String findByName(@PathVariable String name, Model model) {
		model.addAttribute("newLineChar", '\n');
		model.addAttribute("nfo", articleRepository.findByName(name).get(0));
		return "book";
	}

	@GetMapping("/add")
	public String addForm(Model model) {
		return "add";
	}

	@GetMapping("/edit/{name}")
	public String update(@PathVariable String name, Model model) {
		model.addAttribute("nfo", articleRepository.findByName(name).get(0));
		return "edit";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable long id) {
		articleRepository.deleteById(id);
		return "redirect:/articles";
	}
	
	
	@PostMapping
	public String create(Article article) {
		articleRepository.save(article);
		return "redirect:/articles";
	}

}