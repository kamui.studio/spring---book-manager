package fr.ajc.spring.marc.tp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Article {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(nullable = false, unique = true)
	private String name;

	@Column(nullable = false)
	private String editor;

	@Column(nullable = false)
	private String author;

	@Column(nullable = false)
	private String cover;
	
	@Column(nullable = false)
	private Float price;
	
	@Column(nullable = false, columnDefinition="TEXT")
	private String synopsis;

	
	public Article(String name, String editor, String author, String cover, Float price, String synopsis) {
		this.name = name;
		this.editor = editor;
		this.author = author;
		this.cover = cover;
		this.price = price;
		this.synopsis = synopsis;
	}

	public Article() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public String getSynopsis() {
		return synopsis;
	}

	public String getSynopsisHTML() {
		return synopsis.replaceAll("\n", "<br>");
	}

	public void setSynopsis(String synopsis) {
		this.synopsis = synopsis;
	}

	public String toString() {
		return "Article [id=" + id + ", name=" + name + ", editor=" + editor + ", author=" + author + ", cover=" + cover + ", price=" + price
				+ ", synopsis=" + synopsis + "]";
	}
	
}
