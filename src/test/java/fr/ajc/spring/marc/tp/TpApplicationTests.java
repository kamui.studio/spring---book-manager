package fr.ajc.spring.marc.tp;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.util.UUID;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TpApplicationTests {

	private static final String API_ROOT = "http://localhost:8081/api/articles";

	@Test
	public void contextLoads() {
	}

	@Test
	public void whenGetAllArticles_thenOK() {
		Response response = RestAssured.get(API_ROOT);
		System.out.println(response.body().asString());
		assertEquals(HttpStatus.OK.value(), response.getStatusCode());
	}


	@Test
	public void whenGetArticleByName_thenOK() {
		Article article = createRandomArticle();
		createArticleAsUri(article);
		Response response = RestAssured.get(API_ROOT + "/" + article.getName());
		assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
	}
	
	
	// Create a random article with random name and random price
	private Article createRandomArticle() {
		Float randomPrice = (float) (100 + Math.random() * (1000 - 100));
		String randomName = UUID.randomUUID().toString();
		return new Article(randomName, randomName, randomName, randomName, randomPrice, randomName);
	}


	private String createArticleAsUri(Article article) {
		Response response =
		RestAssured.given().contentType(MediaType.APPLICATION_JSON_VALUE).body(article).post(API_ROOT);
		return API_ROOT + "/" + response.jsonPath().get("id");
	}
	
}
